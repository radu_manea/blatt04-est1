## General info
The library system takes care of all the library proceses.
It can import customers, books and bookcopys, delete them from the database or search for them. It can borrow a bookcopy, bring a bookcopy back and more. 
	
## Technologies
Project is created with:
* Java: 1.8
* Maven: 3.6.3
* JUnit: jupiter:5.7
	
## Commands for testing and building:

#test

mvn clean test

#create executable jar

mvn clean install

#build project

mvn build

